# LAG ZS 24/25



## Intro

Milé studentky, milí studenti,

Na této "stránce" najdete info k mým cvíčením předmětu B0B01LAG (Lineární algebra).

Cvičení vedu konzultačním stylem, což znamená, že dostanete úkol k samostatnému vypracování. Během cvičení budu procházet mezi vámi a radit, ale nebudu přímo na tabuli psát řešení úloh. Někdy po cvičení zveřejním i vypracovaná řešení některých úloh ze cvičení. Tento přístup od vás vyžaduje samostatnost a proaktivitu. Zároveň vám však nechávám volnost, abyste si zvolili způsob učení, který vám vyhovuje. Více informací naleznete v sekci [Podmínky pro získání zápočtu](#podminky).

Moje paralelky: 

- 211C, pondělí 11:00, T2:C4-155
- 212C, pondělí 12:45, T2:C4-364

Doporučení:

Lineární algebra je abstraktní obor matematiky a některé pojmy mohou znít nepřirozeně. Proto je užitečné si vytvořit intuitivní představu k formálním definicím. V tomto směru velmi doporučuji vynikající playlist od [3Blue1Brown](https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab), který krásně ilustruje abstraktní pojmy z lineární algebry.

## Konzultace
- osobně před/po cvičením
- e-mail: dunina (.) daria (at) fel (.) cvut (.) cz

## Podmínky pro získání zápočtu <a name="podminky"></a>

- Úspěšné napsání zápočtových testů . Více na [stránce předmětu](https://math.fel.cvut.cz/en/people/velebil/teaching/b0b01lag.html).
- Na cvičení dostanete sadu úloh k samostatnému vypracování. Jedna z úloh bude označená **[Povinná]**, vypracování této úlohy odpovídá přítomnosti na cvičení. Tuto úlohu mi můžete ukázat rovnou na cvičení, nebo poslat mi na e-mail během týdne, tj **před** začátkem dálšího cvičení.
- **Nekontroluji fyzickou přítomnost** na běžných cvičeních. Fyzická přítomnost je vyžadována pouze na cvičeních, během nichž se píše písemka. Předběžný datum najdete na [stránce předmětu](https://math.fel.cvut.cz/en/people/velebil/teaching/b0b01lag.html).

## Cvičení

1. 23.9. Operace, zobrazení, polynomy. [Zadání](/pdf/1cv.pdf), [Řešení](/pdf/1cv_reseni.pdf).
2. 30.9. Důkazy, tělesa a vektorové prostory, pomocné výpočty. [Zadání](/pdf/2cv.pdf), [Řešení](/pdf/2cv_castecna_reseni.pdf).
3. 6.10. Lineární obaly, podprostory, (ne)závislost. [Zadání](/pdf/3cv.pdf), [Řešení](/pdf/3cv_castecna_reseni.pdf).
4. 14.10. Generátory, báze a souřadnice. [Zadání](/pdf/4cv.pdf), [Řešení](/pdf/4cv_castecna_reseni.pdf).
5. 21.10. Lineární zobrazení. [Zadání](/pdf/5cv.pdf), [Řešení](/pdf/5cv_reseni.pdf).
6. 4.11. Matice & lineární zobrazení. [Zadání](/pdf/6cv.pdf), [Řešení](/pdf/6cv_reseni.pdf).
7. 11.11. Ještě matice; jádro a obraz; typy lineárních zobrazení.  [Zadání](/pdf/7cv.pdf), [Řešení](/pdf/7cv_reseni.pdf).
8. 18.11. Písemka. Jak použít matice v lineární algebře? [Zadání](/pdf/8cv.pdf).
9. 25.11. Náhled písemky. Jak použít matice v lineární algebře? Determinanty - začátek. [Zadání](/pdf/9cv.pdf).
10. Samostatné cvičení kvůli nemoci. Determinanty - pokračování. [Zadání](/pdf/10cv.pdf), [Řešení](/pdf/10cv_reseni_bez_povinne.pdf). 
11. 9.12. Determinanty -- závěr. Vlastní čísla, podprostory. Diagonalisace matic. [Zadání](/pdf/11cv.pdf).
12. 16.12. Písemka. Pokračování diagonalisace.
13. 6.1. Skalární součiny. [Zadání](/pdf/13cv.pdf).

## Semestrální testy
1. semestrální test proběhne 18.11. na odpovídajících cvičeních. Tady můžete nahlednout test, co se psál v dřívějším semestru. [Ukázka](/pdf/test1.pdf), [Výsledky](https://docs.google.com/spreadsheets/d/1gJaCxMBQEEchAQIGBo9Y8uX5MFFoAJCFSEKNgeKB1ZI/edit?usp=sharing).
2. semestrální test proběhne 16.12. na odpovídajících cvičeních. Tady můžete nahlednout test, co se psál v dřívějším semestru. [Ukázka](/pdf/test2.pdf), [Výsledky](https://docs.google.com/spreadsheets/d/1v-_oTbrfSwaqZL9OV3h1UujIeDw7eSs-uO5pXnq0jp0/edit?usp=sharing).
